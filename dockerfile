# FROM alpine
FROM arm32v7/python

LABEL maintainer=daryl@freshsystems.co.za

RUN apt-get update && apt-get install -y \ 
    libusb-1.0-0 \
 && rm -rf /var/lib/apt/lists/*
RUN pip3 install pyusb crc16

RUN mkdir -p /usr/src/inverter_upload

COPY src/*.py /usr/src/inverter_upload/ 

ENTRYPOINT [ "python3", "/usr/src/inverter_upload/inverter.py" ]
