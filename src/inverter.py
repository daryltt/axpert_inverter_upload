from inverter_wrapper import Inverter_Wrapper
from thingspeak import Thingspeak
import pvoutput
import time
import argparse
import sys


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('app_name')
    parser.add_argument('--thingspeak', help='Thingspeak key')

    return parser.parse_args(sys.argv)

def thingspeak_upload(resp):
    if (thingspeak != None):
        upload_dict = {
                'field1': resp['grid_voltage'].value,
                'field2': resp['grid_frequency'].value,
                'field3': resp['output_load_percent'].value,
                'field4': resp['ac_output_active_power'].value,
                'field5': resp['battery_voltage'].value
                }
        thingspeak.upload(upload_dict)

args = get_parser()

if args.thingspeak != None:
    thingspeak = Thingspeak(args.thingspeak)
else:
    thingspeak = None

inverter = Inverter_Wrapper()
inverter.init_usb()
inverter.clear_buffer()

while True:
    resp = inverter.send_device_general_status_parameters_inquiry()
    
    if not resp:
        # break
        inverter.clear_buffer()
        pass
    else:
        thingspeak_upload(resp)
        
#         pv_input_current_battery = float(resp['pv_input_current_battery'].value)
#         pv_input_voltage = float(resp['pv_input_voltage'].value)
#         pv_input_power = pv_input_current_battery * pv_input_voltage
        
#         # PVOutput
#         timeOfReading = time.localtime()
#         params = {'d' : time.strftime('%Y%m%d',timeOfReading),
#             't' : time.strftime('%H:%M', timeOfReading),
# ##           'v1' : energyGen,  # for later use if required
#             'v2' : pv_input_power,
# ##           'v3' : energyUse,  # for later use if required
#             'v4' : resp['ac_output_active_power'].value,
# ##           'v5' : temp,       # for later use if required
#             'v6' : resp['pv_input_voltage'].value,
#             'c1' : 0,
#             'n' : 0}
#         pvoutput.upload(params)
        
        
        time.sleep(30)