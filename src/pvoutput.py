import http.client
import urllib.parse

pvo_key = 'YOURKEY'
pvo_systemid = 'YOURSYSID'
pvo_host = 'pvoutput.org'
pvo_statusuri= '/service/r2/addstatus.jsp'

def upload(fields):
    try:
        params_dict = fields.copy()
        params = urllib.parse.urlencode(params_dict) 
        headers = {'X-Pvoutput-Apikey' : pvo_key, 'X-Pvoutput-SystemId' : pvo_systemid,
                   "Accept" : "text/plain", "Content-type": "application/x-www-form-urlencoded"}
        conn = http.client.HTTPConnection(pvo_host)
#        conn.set_debuglevel(2) # debug purposes only
        conn.request("POST", pvo_statusuri, params, headers)
        response = conn.getresponse()
        print("Status", response.status, "   Reason:", response.reason, "-", response.read())
        conn.close()
        return response.status == 200
    except Exception as e:
        print("Exception posting results\n", e)
        return False