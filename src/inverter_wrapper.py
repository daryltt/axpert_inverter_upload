import pdb
from usb_comms import Usb_Comms
import re

class Output_Parameter():
    def __init__(self, name, value, symbol):
        self.name = name
        self.value = value
        self.symbol = symbol
    
    def __str__(self):
        return str(self.name) + ': '+str(self.value)+' ' +str(self.symbol)
    
class Inverter_Wrapper():
    def __init__(self):
        self.usb = None
    
    def init_usb(self):
        self.usb = Usb_Comms(0x0665, 0x5161)
    
    def clear_buffer(self):
        resp = self.usb.getResult()
        print ('Cleared buffer: '+str(resp))
    
    def send_device_general_status_parameters_inquiry(self):
        command = 'QPIGS'
        
        print ('Sending command '+str(command))
        self.usb.sendCommand(command)
        res = self.usb.getResult()
        print ('Result: '+str(res))
        
        if res:
            if res[0] == '(':
                res = res[1:]
            
            res_split = res.split(' ')
            if len(res_split) == 21:
                dict = {
                    'grid_voltage':Output_Parameter('Grid Voltage', res_split[0], 'V'),
                    'grid_frequency':Output_Parameter('Grid Frequency', res_split[1], 'Hz'),
                    'ac_output_voltage':Output_Parameter('AC Output Voltage', res_split[2], 'V'),
                    'ac_output_frequency':Output_Parameter('AC Output Frequency', res_split[3], 'Hz'),
                    'ac_output_apparent_power':Output_Parameter('AC Output Apparent Power', res_split[4], 'W'),
                    'ac_output_active_power':Output_Parameter('AC Output Active Power', res_split[5], 'W'),
                    'output_load_percent':Output_Parameter('Output Load Percent', res_split[6], '%'),
                    'bus_voltage':Output_Parameter('BUS voltage', res_split[7], 'V'),
                    'battery_voltage':Output_Parameter('Battery Voltage', res_split[8], 'V'),
                    'battery_charging_current':Output_Parameter('Battery Charging Current', res_split[9], 'A'),
                    'battery_capacity':Output_Parameter('Battery Capacity', res_split[10], '%'),
                    'heatsink_temperature':Output_Parameter('Inverter Heat Sink Temperature', res_split[11], 'C'),
                    'pv_input_current_battery':Output_Parameter('PV Input Current for Battery', res_split[12], 'A'),
                    'pv_input_voltage':Output_Parameter('PV Input Voltage 1', res_split[13], 'V'),
                    'battery_voltage_scc':Output_Parameter('Battery voltage from SCC', res_split[14], 'V'),
                    'device_status':Output_Parameter('Device Status', res_split[15], ''),
                    'unknown1':Output_Parameter('Unknown1', res_split[16], ''),
                    'unknown2':Output_Parameter('Unknown2', res_split[17], ''),
                    'unknown3':Output_Parameter('Unknown3', res_split[18], ''),
                    'unknown4':Output_Parameter('Unknown4', res_split[19], ''),
                    'unknown5':Output_Parameter('Unknown5', res_split[20], ''),
                }
                return dict
            else:
                print ('Invalid array length: '+str(len(res_split)))
                return False
        else:
            print ('Invalid response: '+str(res))
            return False