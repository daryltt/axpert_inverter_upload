import usb.core, usb.util, usb.control
import crc16
import pdb

class Usb_Comms():
    def __init__(self, vendorId, productId):
        self.vendorId = vendorId
        self.productId = productId
        self.interface = 0
        
        self.dev = usb.core.find(idVendor=vendorId, idProduct=productId)
        if self.dev.is_kernel_driver_active(self.interface):
            self.dev.detach_kernel_driver(self.interface)
        self.dev.set_interface_altsetting(0,0)

    def sendCommand(self, command):
        cmd = command.encode('utf-8')
        crc = crc16.crc16xmodem(cmd).to_bytes(2,'big')
        cmd = cmd+crc
        cmd = cmd+b'\r'
        while len(cmd)<8:
            cmd = cmd+b'\0'
    
        self.dev.ctrl_transfer(0x21, 0x9, 0x200, 0, cmd)

    def getResult(self, timeout=100):
        res=""
        i=0
        while '\r' not in res and i<20:
            try:
                res+="".join([chr(i) for i in self.dev.read(0x81, 8, timeout) if i!=0x00])
            except usb.core.USBError as e:
                if e.errno == 110:
                    print ('errno == 110 '+str(e))
                    pass
                else:
                    raise
            i+=1
        return res