import http.client
import urllib.parse

class Thingspeak():
    def __init__(self, key):
        self.key = key

    def upload(self, fields):
        print('Uploading to thingspeak: ' + str(fields))
        
        params_dict = fields.copy()
        params_dict['key'] = self.key
        params = urllib.parse.urlencode(params_dict) 
        headers = {"Content-type": "application/x-www-form-urlencoded","Accept": "text/plain"}
        conn = http.client.HTTPConnection("api.thingspeak.com:80")
        try:
            conn.request("POST", "/update", params, headers)
            response = conn.getresponse()
            print (response.status, response.reason)
            data = response.read()
            conn.close()
        except:
            print ("Connection Failed")