# Axpert Inverter Upload
Using a Raspberry Pi to upload data from an Axpert/Voltronic Inverter to the cloud.

I created this basic project because there seems to be many unanswered questions about getting this to work.

* Go install [Raspbian Stretch Lite](https://www.raspberrypi.org/downloads/raspbian/)
* Run [raspi-config](https://www.raspberrypi.org/documentation/configuration/raspi-config.md) to set up wifi and ssh
* Run `curl -sSL https://get.docker.com | sh` to [install docker](https://www.raspberrypi.org/blog/docker-comes-to-raspberry-pi/)

## Thingspeak

* Go set up a [thingspeak](https://thingspeak.com/) channel.
* The fields must be set up as follows:
  * Grid Voltage
  * Grid Frequency
  * Output Load Percent
  * Output Power
  * Battery Voltage

[Here is my channel](https://thingspeak.com/channels/263915)

## To Run

`docker run -d --privileged -v /dev/bus/usb:/dev/bus/usb daryltt/rpi_axpert_inverter_upload --thingspeak <THINGSPEAK KEY>`

## To Do

* Add CI Process and tests
* Make a mapping file to be able to customise the thingspeak upload
* Finish upload for PV Output
* Include tinydb cache for when the internet is not available
* Add coulomb counting